WHITE = 1
BLACK = 2


def opponent(color):
    return BLACK if color == WHITE else WHITE


def correct_coords(row, col):
    return 0 <= row < 8 and 0 <= col < 8


class Board:
    def __init__(self):
        self.color = WHITE
        self.field = []

        for row in range(8):
            self.field.append([None] * 8)

        self.field[1][4] = Pawn(1, 4, WHITE)

    def get_color(self):
        return self.color

    def cell(self, row, col):
        piece = self.field[row][col]
        if piece is None:
            return '  '
        color = piece.get_color()
        c = 'w' if color == WHITE else 'b'

        return c + piece.char()

    def is_under_attack(self, row, col, color):
        for i in range(8):
            for j in range(8):
                if self.field[i][j] is not None:
                    piece = self.field[i][j]
                    if piece.get_color() == color:
                        if piece.can_move(row, col):
                            return True
        return False

    def move_piece(self, row, col, row1, col1):
        if not correct_coords(row, col) or not correct_coords(row1, col1):
            return False
        if row == row1 and col == col1:
            return False  # нельзя пойти в ту же клетку
        piece = self.field[row][col]
        if piece is None:
            return False
        if piece.get_color() != self.color:
            return False
        if not piece.can_move(row1, col1):
            return False
        self.field[row][col] = None  # Снять фигуру.
        self.field[row1][col1] = piece  # Поставить на новое место.
        piece.set_position(row1, col1)
        self.color = opponent(self.color)
        return True


class Pawn:
    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row, self.col = row, col

    def char(self):
        return 'p'

    def get_color(self):
        return self.color

    def can_move(self, row, col):
        if self.col != col:
            return False

        direction, start_row = (1, 1) if self.color == WHITE else (-1, 6)

        if self.row + direction == row:
            return True

        if self.row == start_row and self.row + 2 * direction == row:
            return True

        return False


class Rook:

    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row, self.col = row, col

    def char(self):
        return "R"

    def get_color(self):
        return self.color

    def can_move(self, row, col):
        if self.row != row and self.col != col:
            return False

        return True


class Queen:
    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row, self.col = row, col

    def char(self):
        return "Q"

    def get_color(self):
        return self.color

    def can_move(self, row, col):
        if not (0 <= row <= 7 and 0 <= col <= 7):
            return False
        ar = abs(self.row - row)
        ac = abs(self.col - col)
        return ar == ac or ar * ac == 0


class Bishop:
    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row, self.col = row, col

    def char(self):
        return "B"

    def get_color(self):
        return self.color

    def can_move(self, row, col):
        return abs(self.row - row) == abs(self.col - col)


class Knight:
    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row, self.col = row, col

    def char(self):
        return "B"

    def get_color(self):
        return self.color

    def can_move(self, row, col):
        ar = abs(self.row - row)
        ac = abs(self.col - col)

        return ar == 2 and ac == 1 or ar == 1 and ac == 2
