a = int(input())
count = 0
while a > 1:
    a = a / 2 if a % 2 == 0 else a - 1
    count += 1

print(count)
