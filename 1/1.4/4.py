count = 0
min = 0
max = 200

height = input()

while height != "!":
    height = int(height)
    if 150 <= height <= 190:
        count += 1
        if min < height:
            min = height
        if max > height:
            max = height
    height = input()

print(count)
print(max, min)
