price = 0
n = float(input())
while n >= 0:
    if n > 1000:
        price += (n * 0.95)
    else:
        price += n
    n = float(input())
print(price)
