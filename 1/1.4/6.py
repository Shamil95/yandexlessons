xpos = 0
ypos = 0
dir = "север"
count = 0
xklad = int(input())
yklad = int(input())

while xpos != xklad or ypos != yklad:
    oper = input()
    count += 1
    if oper == "вперёд":
        steps = int(input())

        if dir == "север":
            ypos += steps
        elif dir == "юг":
            ypos -= steps
        elif dir == "восток":
            xpos += steps
        elif dir == "запад":
            xpos -= steps
    elif oper == "налево":
        if dir == "север":
            dir = "запад"
        elif dir == "запад":
            dir = "юг"
        elif dir == "юг":
            dir = "восток"
        else:
            dir = "север"
    elif oper == "направо":
        if dir == "север":
            dir = "восток"
        elif dir == "восток":
            dir = "юг"
        elif dir == "юг":
            dir = "запад"
        else:
            dir = "север"
    elif oper == "разворот":
        if dir == "запад":
            dir = "восток"
        elif dir == "восток":
            dir = "запад"
        elif dir == "север":
            dir = "юг"
        else:
            dir = "север"
    elif oper == "стоп":
        break

print(count)
print(dir)
