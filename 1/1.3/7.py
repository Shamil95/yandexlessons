n = int(input())

f = n // 100
s = n % 100 // 10
t = n % 10

fn = f + s
sn = s + t

if fn > sn:
    res = str(fn) + str(sn)
else:
    res = str(sn) + str(fn)

print(res)
