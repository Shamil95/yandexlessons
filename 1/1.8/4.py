n = int(input())
maxHeight = 0
iDor = 0
for i in range(n):
    m = 0
    nDor = int(input())
    for j in range(nDor):
        height = int(input())
        if m == 0:
            m = height

        if m > height:
            m = height
    if maxHeight < m:
        maxHeight = m
        iDor = i + 1
print(iDor, maxHeight)
