n = int(input())
count = 0
success = True
while n > 0:
    if not success:
        count = 0
        n -= 1
        if n == 0:
            break
        success = True
    n1 = input()
    if n1 != "раз":
        print("Правильных отсчётов было", str(count) + ", но теперь вы ошиблись.")
        success = False
        continue
    count += 1
    n2 = input()
    if n2 != "два":
        print("Правильных отсчётов было", str(count) + ", но теперь вы ошиблись.")
        success = False
        continue
    count += 1
    n3 = input()
    if n3 != "три":
        print("Правильных отсчётов было", str(count) + ", но теперь вы ошиблись.")
        success = False
        continue
    count += 1
    n4 = input()
    if n4 != "четыре":
        print("Правильных отсчётов было", str(count) + ", но теперь вы ошиблись.")
        success = False
        continue
    count += 1

print("На сегодня хватит.")
