country_first = "Евразия"
country_second = "Остазия"

b = True

n = int(input())

for _ in range(n):
    question = input()

    if question == "С кем война?":
        print("Евразия" if b else "Остазия")
    elif question == "Меняем":
        b = not b
    else:
        print("Остазия" if b else "Евразия")