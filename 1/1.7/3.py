count = 0
b = True
while True:
    st = input()
    if st == "СТОП":
        break
    if b:
        count += 1
    else:
        continue
    if "кот" in st.lower():
        b = False
if b:
    print(-1)
else:
    print(count)