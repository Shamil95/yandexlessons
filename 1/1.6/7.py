num, denum = 0, 1
n = int(input())
for i in range(n):
    a, b = int(input()), int(input())
    num = num * b + a * denum
    denum *= b
x, y = num, denum
while y > 0:
    x, y = y, x % y
print(num // x, '/', denum // x, sep='')