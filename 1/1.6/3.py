n = int(input())
c = 0
for i in range(1, n + 1):
    if i == n:
        print(i)
    elif n % i == 0:
        print(i, end=" ")
    c += 1
if c > 2:
    print("НЕТ")
else:
    print("ПРОСТОЕ")