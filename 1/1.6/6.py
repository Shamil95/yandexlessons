n = int(input())
pi = 3.141592653589793 ** 2
sum = 0

for i in range(1, n + 1):
    sum = sum + (1 / i ** 2)

print(pi / sum)